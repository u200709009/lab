public class PrimeNumbers{
    public static void main (String[] args){
        int max = Integer.parseInt(args[0]);

        //Print the numbers less than max
        //For each number less than max
        for ( int number = 2 ; number <  max ; number ++) {


            // Check if the number is prime or not
            // Let divisor = 2;
            int divisor = 2;
            // let isprime = true;
            boolean isPrime = true;
            // while divisor less than number
            while (divisor < number && isPrime ) {
                // if the number is divisible by divisor
                if (number % divisor == 0)
                    // number is not prime  // isprime = false
                    isPrime = false;

                //increament divisor
                divisor++;
            }
                //if the number is prime
            if ( isPrime)
            //print a number
                System.out.println(number + " ");
        }

    }
}