public class Point2 {
    int xCoordinate;
    int yCoordinate;
    static int count;
    static double pi=3.14;
    public Point2(int x, int y){
        this.xCoordinate = x;
        this.yCoordinate = y;
        count ++;
    }
    public void move(int xDistance, int yDistance){
        xCoordinate += xDistance;
        yCoordinate += yDistance;
        }
    public double distanceFromOrigin(){
        return Math.sqrt(xCoordinate*xCoordinate+yCoordinate*yCoordinate);
    }
    public double distanceFromPoint(Point2 pointx){
        int xDiff = xCoordinate-pointx.xCoordinate;
        int yDiff = yCoordinate-pointx.yCoordinate;
        return Math.sqrt(xDiff*xDiff+yDiff*yDiff);

    }
}


